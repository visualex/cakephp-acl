# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.16)
# Database: acl_plugin
# Generation Time: 2014-05-08 12:54:08 +0200
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `acos`;

CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,NULL,NULL,'controllers',1,74),
	(2,1,NULL,NULL,'Groups',2,13),
	(3,2,NULL,NULL,'index',3,4),
	(4,2,NULL,NULL,'view',5,6),
	(5,2,NULL,NULL,'add',7,8),
	(6,2,NULL,NULL,'edit',9,10),
	(7,2,NULL,NULL,'delete',11,12),
	(8,1,NULL,NULL,'Pages',14,17),
	(9,8,NULL,NULL,'display',15,16),
	(10,1,NULL,NULL,'Posts',18,29),
	(11,10,NULL,NULL,'index',19,20),
	(12,10,NULL,NULL,'view',21,22),
	(13,10,NULL,NULL,'add',23,24),
	(14,10,NULL,NULL,'edit',25,26),
	(15,10,NULL,NULL,'delete',27,28),
	(16,1,NULL,NULL,'Users',30,45),
	(17,16,NULL,NULL,'login',31,32),
	(18,16,NULL,NULL,'logout',33,34),
	(19,16,NULL,NULL,'index',35,36),
	(20,16,NULL,NULL,'view',37,38),
	(21,16,NULL,NULL,'add',39,40),
	(22,16,NULL,NULL,'edit',41,42),
	(23,16,NULL,NULL,'delete',43,44),
	(24,1,NULL,NULL,'Widgets',46,57),
	(25,24,NULL,NULL,'index',47,48),
	(26,24,NULL,NULL,'view',49,50),
	(27,24,NULL,NULL,'add',51,52),
	(28,24,NULL,NULL,'edit',53,54),
	(29,24,NULL,NULL,'delete',55,56),
	(30,1,NULL,NULL,'AclManager',58,73),
	(31,30,NULL,NULL,'Acl',59,72),
	(32,31,NULL,NULL,'drop',60,61),
	(33,31,NULL,NULL,'drop_perms',62,63),
	(34,31,NULL,NULL,'index',64,65),
	(35,31,NULL,NULL,'permissions',66,67),
	(36,31,NULL,NULL,'update_acos',68,69),
	(37,31,NULL,NULL,'update_aros',70,71);

/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros`;

CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`)
VALUES
	(1,NULL,'Group',1,NULL,1,4),
	(2,NULL,'Group',2,NULL,5,8),
	(3,NULL,'Group',3,NULL,9,10),
	(4,1,'User',1,NULL,2,3),
	(5,2,'User',2,NULL,6,7);

/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table aros_acos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `aros_acos`;

CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`)
VALUES
	(1,1,1,'1','1','1','1'),
	(2,2,1,'1','1','1','1'),
	(3,3,1,'0','0','0','0'),
	(4,1,2,'1','1','1','1'),
	(5,2,2,'-1','-1','-1','-1'),
	(6,3,2,'0','0','0','0'),
	(7,1,3,'1','1','1','1'),
	(8,2,3,'0','0','0','0'),
	(9,3,3,'0','0','0','0'),
	(10,1,4,'1','1','1','1'),
	(11,2,4,'0','0','0','0'),
	(12,3,4,'0','0','0','0'),
	(13,1,5,'1','1','1','1'),
	(14,2,5,'0','0','0','0'),
	(15,3,5,'0','0','0','0'),
	(16,1,6,'1','1','1','1'),
	(17,2,6,'0','0','0','0'),
	(18,3,6,'0','0','0','0'),
	(19,1,7,'1','1','1','1'),
	(20,2,7,'0','0','0','0'),
	(21,3,7,'0','0','0','0'),
	(22,1,8,'0','0','0','0'),
	(23,2,8,'1','1','1','1'),
	(24,3,8,'0','0','0','0'),
	(25,1,9,'0','0','0','0'),
	(26,2,9,'0','0','0','0'),
	(27,3,9,'0','0','0','0'),
	(28,1,10,'0','0','0','0'),
	(29,2,10,'1','1','1','1'),
	(30,3,10,'0','0','0','0'),
	(31,1,11,'0','0','0','0'),
	(32,2,11,'0','0','0','0'),
	(33,3,11,'0','0','0','0'),
	(34,1,12,'0','0','0','0'),
	(35,2,12,'0','0','0','0'),
	(36,3,12,'0','0','0','0'),
	(37,1,13,'0','0','0','0'),
	(38,2,13,'0','0','0','0'),
	(39,3,13,'0','0','0','0'),
	(40,1,14,'0','0','0','0'),
	(41,2,14,'0','0','0','0'),
	(42,3,14,'0','0','0','0'),
	(43,1,15,'0','0','0','0'),
	(44,2,15,'0','0','0','0'),
	(45,3,15,'0','0','0','0'),
	(46,1,16,'0','0','0','0'),
	(47,2,16,'-1','-1','-1','-1'),
	(48,3,16,'0','0','0','0'),
	(49,1,17,'0','0','0','0'),
	(50,2,17,'0','0','0','0'),
	(51,3,17,'0','0','0','0'),
	(52,1,18,'0','0','0','0'),
	(53,2,18,'1','1','1','1'),
	(54,3,18,'0','0','0','0'),
	(55,1,19,'0','0','0','0'),
	(56,2,19,'0','0','0','0'),
	(57,3,19,'0','0','0','0'),
	(58,1,20,'0','0','0','0'),
	(59,2,20,'0','0','0','0'),
	(60,3,20,'0','0','0','0'),
	(61,1,21,'0','0','0','0'),
	(62,2,21,'0','0','0','0'),
	(63,3,21,'0','0','0','0'),
	(64,1,22,'0','0','0','0'),
	(65,2,22,'0','0','0','0'),
	(66,3,22,'0','0','0','0'),
	(67,1,23,'0','0','0','0'),
	(68,2,23,'0','0','0','0'),
	(69,3,23,'0','0','0','0'),
	(70,1,24,'0','0','0','0'),
	(71,2,24,'-1','-1','-1','-1'),
	(72,3,24,'0','0','0','0'),
	(73,1,25,'0','0','0','0'),
	(74,2,25,'0','0','0','0'),
	(75,3,25,'0','0','0','0'),
	(76,1,26,'0','0','0','0'),
	(77,2,26,'0','0','0','0'),
	(78,3,26,'0','0','0','0'),
	(79,1,27,'0','0','0','0'),
	(80,2,27,'0','0','0','0'),
	(81,3,27,'0','0','0','0'),
	(82,1,28,'0','0','0','0'),
	(83,2,28,'0','0','0','0'),
	(84,3,28,'0','0','0','0'),
	(85,1,29,'0','0','0','0'),
	(86,2,29,'0','0','0','0'),
	(87,3,29,'0','0','0','0'),
	(88,1,30,'0','0','0','0'),
	(89,2,30,'-1','-1','-1','-1'),
	(90,3,30,'0','0','0','0'),
	(91,1,31,'0','0','0','0'),
	(92,2,31,'0','0','0','0'),
	(93,3,31,'0','0','0','0'),
	(94,1,32,'0','0','0','0'),
	(95,2,32,'0','0','0','0'),
	(96,3,32,'0','0','0','0'),
	(97,1,33,'0','0','0','0'),
	(98,2,33,'0','0','0','0'),
	(99,3,33,'0','0','0','0'),
	(100,1,34,'0','0','0','0'),
	(101,2,34,'0','0','0','0'),
	(102,3,34,'0','0','0','0'),
	(103,1,35,'0','0','0','0'),
	(104,2,35,'0','0','0','0'),
	(105,3,35,'0','0','0','0'),
	(106,1,36,'0','0','0','0'),
	(107,2,36,'0','0','0','0'),
	(108,3,36,'0','0','0','0'),
	(109,1,37,'0','0','0','0'),
	(110,2,37,'0','0','0','0'),
	(111,3,37,'0','0','0','0');

/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `created`, `modified`)
VALUES
	(1,'Administrators','2014-05-07 13:50:29','2014-05-07 13:50:29'),
	(2,'Archivist','2014-05-07 13:50:53','2014-05-07 13:50:53'),
	(3,'Collectionist','2014-05-07 13:51:02','2014-05-07 13:51:02');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `password`, `group_id`, `created`, `modified`)
VALUES
	(1,'user1','4df8bc6f81acbb91d9eb1bad0007b11a1c9fd77f',1,'2014-05-07 13:51:22','2014-05-08 12:37:17'),
	(2,'user2','cfc9463b8e1a8f92e0d3632e96279ab9ec926f0c',2,'2014-05-07 13:51:37','2014-05-08 12:37:29');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `part_no` varchar(12) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
